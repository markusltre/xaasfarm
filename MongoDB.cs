﻿using System;
using System.Security.Cryptography;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace XaaSfarm
{
    public class MongoDB
    {
        private readonly IMongoDatabase db;
        private readonly string table;

        /**
         * Constructor for MongoDB
         * connectionString, database and table can be set in the config.json file.
         */
        public MongoDB(string connectionString, string database, string table)
        {
            IMongoClient client = new MongoClient(connectionString);
            db = client.GetDatabase(database);
            this.table = table;
        }

        /**
         * InsertTweet: inserts a tweet and the result of the sentiment analysis
         * into the MongoDB Database.
         */
        public void InsertTweet(string tweet, double sentimentScore)
        {
            TweetModel model = new TweetModel { Id = GenerateGuid(tweet), Tweet = tweet, Score = sentimentScore };
            UpsertRecord<TweetModel>(model.Id, model);
            //InsertRecord<TweetModel>(model);
        }

        /**
         * Generates a Guid object, to avoid dublicate entries in the database.
         * -> Same tweet gets same Guid object.
         */
        public Guid GenerateGuid(string tweet)
        {
            byte[] hash = MD5.Create().ComputeHash(Encoding.Default.GetBytes(tweet));
            return new Guid(hash);
        }

        /**
         * Inserts a new Entry / Document in the Database, if the object id does
         * not exist. -> else: update existing file.
         */
        public void UpsertRecord<T>(Guid id, T record)
        {
            var collection = db.GetCollection<T>(this.table);

            collection.ReplaceOne(
                new BsonDocument("_id", id),
                record,
                new ReplaceOptions { IsUpsert = true });
        }
    }

    /**
     * Tweet objects that are stored in the database.
     */
    public class TweetModel
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Tweet { get; set; }
        public double Score { get; set; }
    }
}
