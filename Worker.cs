using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System.IO;

namespace XaaSfarm
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        private HttpClient client;
        private Twitter twitterClient;
        private Azure azureClient;
        private MongoDB mongoDB;

        private readonly dynamic config;


        public Worker(ILogger<Worker> logger)
        {
            //initialize Settings from Properites/config.json
            using (StreamReader r = new StreamReader("Properties/config.json"))
            {
                string json = r.ReadToEnd();
                this.config = JObject.Parse(json);
            }

            _logger = logger;
        }

        /**
         * initialize Azure-, Twitter-, MongoDB- and Http-Clients
         */
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            client = new HttpClient();

            azureClient = new Azure(
                (string)config.authentication.microsoftAzure.textAnalytics.endpoint,
                (string)config.authentication.microsoftAzure.textAnalytics.key
                );

            twitterClient = new Twitter(
                (string)config.authentication.twitter.consumerKey,
                (string)config.authentication.twitter.consumerKeySecret,
                (string)config.authentication.twitter.accessToken,
                (string)config.authentication.twitter.accessTokenSecret
                );

            mongoDB = new MongoDB(
                (string)config.authentication.mongodb.atlasConnectionString,
                (string)config.authentication.mongodb.database,
                (string)config.authentication.mongodb.table
                );

            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                String[] tweets = GetTwitterPosts("%23" + (string)config.appData.hashtag, (int)config.appData.count); //%23 -> #
                foreach (string tweet in tweets)
                {
                    _logger.LogInformation(tweet); //Log the tweet in the Application Output
                    double score = azureClient.SentimentAnalysis(tweet); //get Sentiment score
                    _logger.LogInformation($"Sentiment Score: {score:0.00}");
                    mongoDB.InsertTweet(tweet, score); //save tweet and result in the MongoDB database

                }
                await Task.Delay((int)(((double) config.appData.repeteTime) * 60.0 * 60.0 * 1000.0), stoppingToken); //The first number indicates the execution interval in hours
            }
        }

        public String[] GetTwitterPosts(String searchValue, int quantity)
        {
            String result = twitterClient.GetTweets(searchValue, quantity);
            dynamic posts = JObject.Parse(result);
            String[] tweets = new String[posts.statuses.Count];

            for (int i = 0; i < posts.statuses.Count; i++)
            {
                tweets[i] = posts.statuses[i].text;
            }
            return tweets;
        }

        public void PushDataToDatabase()
        {

        }
    }
}
