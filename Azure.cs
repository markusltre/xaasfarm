﻿
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;


namespace XaaSfarm
{
    public class Azure
    {
        private readonly TextAnalyticsClient client;
        private readonly string endpoint;

        public Azure(string endpoint, string key)
        {
            this.endpoint = endpoint;
            this.client = authenticateClient(key);
        }

        /**
         * Creates new TextAnalytics Client
         */
        private TextAnalyticsClient authenticateClient(string key)
        {
            ApiKeyServiceClientCredentials credentials = new ApiKeyServiceClientCredentials(key);
            TextAnalyticsClient client = new TextAnalyticsClient(credentials)
            {
                Endpoint = this.endpoint
            };
            return client;
        }

        /**
         * perform sentimental text analysis and return the value as double.
         * 0.0 means bad mood and 1.0 means good mood. 0.5 is neutral.
         */
        public double SentimentAnalysis(string text)
        {
            var result = this.client.Sentiment(text).Score;

            return (double)result;
        }
    }
}