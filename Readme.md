## About this Project

This program is designed to create mood analyses of Twitter posts on a specific topic and then store them in a MongoDB database. The program was developed as a C# worker service, and therefore runs repeatedly after a specified time (this time can be changed in the config.json file in the Properties folder). This allows a longer period of time to create mood analyses of a topic automatically.

## Getting Started

First, the settings in "/Properties/config.json" must be adjusted. This project uses the Twitter API, the text analysis tool from Microsoft Azure and the MongoDB database. For the program to work, this information must be completed.

### Notes

* The program may not work properly on MacOS because of connection problems with the MongoDB database.
* The text analyses with Microsoft Azure are performed with the version "TextAnalytics-v2-1".